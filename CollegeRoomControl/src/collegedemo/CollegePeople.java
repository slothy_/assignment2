/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collegedemo;

/**
 *
 * @author sh
 */
public abstract class CollegePeople {
    protected int id;
    protected String name;
    protected String type;
    protected Room course;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUserType() {
        return type;
    }

    public Room getCourse() {
        return course;
    }

    public void setCourse(Room course) {
        this.course = course;
    }
    
}
