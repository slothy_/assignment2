/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collegedemo;

/**
 *
 * @author sh
 */
public class Room {
    
    private Lecturer lecturer;
    private String courseWork;
    private String code;
    private String room;
    private String roomType;
    private boolean emergency_mode;

    public Room(String room, String code, String roomType) {
        this.room = room;
        this.code = code;
        this.roomType = roomType;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }
    
    public Lecturer getLecturer() {
        return lecturer;
    }

    public void setLecturer(Lecturer lecturer) {
        this.lecturer = lecturer;
    }

    public String getCourseWork() {
        return courseWork;
    }

    public void setCourseWork(String courseWork) {
        this.courseWork = courseWork;
    }

    public boolean isEmergency() {
        return emergency_mode;
    }

    public void setEmergencyMode(boolean emergency_mode) {
        this.emergency_mode= emergency_mode;
    }

    
    
    
}
