/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collegedemo;
import java.io.File;  // Import the File class
import java.io.IOException;  // Import the IOException class to handle errors
import java.io.FileOutputStream;  
import java.time.LocalDate;
import java.time.LocalTime;
/**
 *
 * @author sh
 */
public class SwipeCard {
    private static String emergencyText;
    private static final String COLOUR_RED = "\u001B[31m";
    private static final String COLOUR_BLUE = "\u001B[34m";
    private static final String COLOUR_RESET = "\u001B[0m";
    private static final String COLOUR_GREEN = "\u001B[32m";
    private static int userid;
    private static String room;
    private static boolean denied;
    private static boolean append = true;

    public static void Swipe(CollegePeople p, Room r) {
        System.out.println(p.getName() + " has swiped the card in room: " + COLOUR_BLUE + r.getRoom() + COLOUR_RESET);

        if (p.getCourse().isEmergency() == true) {
           emergencyText = "during " + COLOUR_RED+"Emergency"+COLOUR_RESET+" mode.";
        } else if(p.getCourse().isEmergency() == false) {
            emergencyText = "during " + COLOUR_GREEN+"Normal"+COLOUR_RESET+" mode.";
        } else {
            emergencyText = "";
        }

        if (p.getCourse().getRoom() != r.getRoom()) {
            System.out.println(p.getName() + " doesn't have access to this room " + emergencyText);
            denied = true;
        } else {
            System.out.println(p.getName() + " has "+COLOUR_GREEN+"gained access"+COLOUR_RESET+" to this room " + emergencyText);
            denied = false;
        }
        try {
          File myObj = new File("roomAccess.txt");
          if (myObj.createNewFile()) {
            System.out.println("File created: " + myObj.getName());
          }
            FileOutputStream fout=new FileOutputStream("roomAccess.txt", append);
            if (denied == true) {
                String s = "\n["+LocalDate.now()+" @ "+LocalTime.now()+"]"+p.getName() + " was REFUSED access to room: " + r.getRoom();
                byte b[]=s.getBytes();
                fout.write(b);
                fout.close();
            } else {
                String s = "\n["+LocalDate.now()+" @ "+LocalTime.now()+"] "+p.getName() + " was GRANTED access to room: " + r.getRoom();
                byte b[]=s.getBytes();
                fout.write(b);
                fout.close();
            }
        } catch (IOException e) {
          System.out.println("An error occurred.");
          e.printStackTrace();
        }
    }
}
