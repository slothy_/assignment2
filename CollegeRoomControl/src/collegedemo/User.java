/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collegedemo;

/**
 *
 * @author sh
 */
public class User extends CollegePeople{
    
    public User(int id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }
    
    public void attendClass() {
        System.out.println(this.name + " is attending " + this.course.getCode() + " in room: " + this.course.getRoom());
    }
    
    public void doCourseWork() {
        
        if(this.course.getCourseWork() != null && !this.course.getCourseWork().isEmpty()) { 
            System.out.println(this.name + " is doing coursework: " + this.course.getCourseWork());
        } else {
            System.out.println("No coursework set!");
        }
    }
}
