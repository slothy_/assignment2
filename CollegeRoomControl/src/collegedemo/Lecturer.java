/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collegedemo;

/**
 *
 * @author sh
 */
public class Lecturer extends CollegePeople implements ITeach {
    public Lecturer(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public void Teach() {
        System.out.println(this.name + " is teaching " + this.course.getCode() + " in room: " + this.course.getRoom());
    }

    public void setCourseWork(String courseWork) {
        this.course.setCourseWork(courseWork);
    
    }   
    
}
