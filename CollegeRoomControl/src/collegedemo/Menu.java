/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collegedemo;

/**
 *
 * @author 10758107
 */
import java.io.File;
import java.util.Scanner; // import the scanner library
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
class Menu {

   private int input;
   Menu()
   {
       input = 0;
   }

   public void setInput(int thisInput)
   {
       input = thisInput;
   }

   public int getInput()
   {
       return input;
   }
   
   int NewRoom = 6;
   int GridHeight = 7;
   
   private static final String COLOUR_RED = "\u001B[31m";
    private static final String COLOUR_BLUE = "\u001B[34m";
    private static final String COLOUR_RESET = "\u001B[0m";
    private static final String COLOUR_GREEN = "\u001B[32m";

   // new User(USER ID, "PERSONS NAME", "USER GROUP")
        // USER TYPES:
        // STAFF
        // STUDENT
        // VISITOR
        // CONTRACT_CLEANER
        // MANAGER
        // SECURITY
        // EMERGENCY_RESPONDER
        User Chris = new User(1, "Dr. Chris Ford", "STAFF");
        
        User Shane = new User(10758107, "Shane Froggatt", "STUDENT");
        
        
        Room KS224 = new Room("KS224", "CITY2108", "TEACHING_ROOM");
        Room KS100 = new Room("KS100", "CITY108", "SECURE_ROOM");
        // new Room("COURSE CODE", "COURSE ROOM")
        // ROOM TYPES:
        // LECTURE_HALL
        // TEACHING_ROOM
        // STAFF_ROOM
        // SECURE_ROOM

        

   
   public void printMainMenu() {
       System.out.println(COLOUR_RED + "   ____              _   ____            _                 ");
       System.out.println(COLOUR_RED + "  / ___|__ _ _ __ __| | / ___| _   _ ___| |_ ___ _ __ ___  ");
       System.out.println(COLOUR_RED + " | |   / _` | '__/ _` | \\___ \\| | | / __| __/ _ \\ '_ ` _ \\ ");
       System.out.println(COLOUR_RED + " | |__| (_| | | | (_| |  ___) | |_| \\__ \\ ||  __/ | | | | |");
       System.out.println(COLOUR_RED + "  \\____\\__,_|_|  \\__,_| |____/ \\__, |___/\\__\\___|_| |_| |_|");
       System.out.println(COLOUR_RED + "                               |___/                       "+COLOUR_RESET);
       System.out.println("");
       System.out.println("Menu Options:");
       System.out.println("1. Campus Users");
       System.out.println("2. Rooms");
       System.out.println("3. Check Room Access Logs");
       System.out.println("4. Swipe Card");
       System.out.println("");
       System.out.print("Please select an option from 1-3\r\n");
       KS224.setEmergencyMode(false);
        KS100.setEmergencyMode(true);
        Admin.assignRoom(Chris, KS100);
        Admin.assignRoom(Shane, KS224);
   }

   public void printSubMenu2() {
       System.out.println("Menu Options:");
       System.out.println("5. Add rooms");
       System.out.println("6. Update rooms *WORK IN PROGRESS*");
       System.out.println("7. Remove rooms *WORK IN PROGRESS*");
       System.out.println("8. Return to main menu");
       System.out.println("");
       System.out.print("Please select an option from 5-7\r\n");
   }

   public void printSubMenu3() {
       try {
           Files.lines(new File("roomAccess.txt").toPath())
                   .map(s -> s.trim())
                   .filter(s -> !s.isEmpty())
                   .forEach(System.out::println);
       } catch (IOException ex) {
           Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
       }
       System.out.print("Press 8 to return to the main menu\r\n");
   }

    public void printSubMenu4() {
       System.out.println("Menu Options:");
       System.out.println("9. Add new user");
       System.out.println("10. Remove user *WORK IN PROGRESS*");
       System.out.println("8. Return to main menu");
       System.out.println("");
       System.out.print("Please select an option from 5-7\r\n");
   }

   public void printSubMenu5() {
       System.out.print("Returning to the main menu.\r\n");
   }
   
   public void setNewRoom() {
       System.out.println("Current List of Campus Rooms:");
       System.out.println("");
       System.out.println("Add a new room:");
       System.out.println("Input the name of the room: ");
       System.out.println("");

       Scanner NewRoomScanner = new Scanner(System.in);
       String NewRoomInput = NewRoomScanner.nextLine();
        
       System.out.println("Input the course code: ");
       String NewRoomInput2 = NewRoomScanner.nextLine();
       
       System.out.println("Input the room type (LECTURE_HALL, TEACHING_ROOM, STAFF_ROOM, SECURE_ROOM) : ");
       String NewRoomInput3 = NewRoomScanner.nextLine();

       System.out.println("");
       System.out.println(NewRoomInput + " has been added as a new room.");
       System.out.println(NewRoomInput2 + " has been added as a new course code.");
       System.out.println(NewRoomInput3 + " has been added as the room type.");

       Room NewRoom = new Room(NewRoomInput2, NewRoomInput, NewRoomInput3);

       System.out.println("");
       System.out.println("Returning to main menu");
       System.out.println("");
       printMainMenu();
        // new Room("COURSE CODE", "COURSE ROOM")
   }
   
   public void setGridHeight() {
       System.out.println("");
       System.out.println("Current List of Campus Users:");
       Admin.getDetails(Shane);
       Admin.getDetails(Chris);
       System.out.println("");
       System.out.println("Add a new user:");
       System.out.println("Input the user ID for the new user: ");

       Scanner NewUserScanner = new Scanner(System.in);
       int NewUserInput = NewUserScanner.nextInt();
        NewUserScanner.nextLine();
       System.out.println("Input the user name: ");
       String NewUserInput2 = NewUserScanner.nextLine();
    
       System.out.println("Input the user type (STAFF, STUDENT, VISITOR, CONTRACT_CLEANER, MANAGER, SECURITY, EMERGENCY_RESPONDER) : ");
       String NewUserInput3 = NewUserScanner.nextLine();
       
       
       
       System.out.println("");
       System.out.println(NewUserInput + " has been added as a new user ID.");
       System.out.println(NewUserInput2 + " has been added as a new user name.");
       System.out.println(NewUserInput3 + " has been added as the user type.");

       User NewUser = new User(NewUserInput, NewUserInput2, NewUserInput3);

       System.out.println("");
       System.out.println("Returning to main menu");
       System.out.println("");
       printMainMenu();
   }
   
   public int getWidth(){
         return NewRoom;
   }
   
   public int getHeight(){
         return GridHeight;
   }

   public void processMenuOptions(int myInput) 
   {
       switch (myInput) {
           case 1:
               System.out.println("Gathering campus users.\r\n"); 
                printSubMenu4(); //calling option 6 object and/or methods
               break;
           case 2:
               System.out.println("\nGathering campus rooms.\r\n");
               printSubMenu2(); //calling match settings submenu
               break;
           case 3:
               System.out.println("\nGathering logs.\r\n");
               printSubMenu3(); //calling help and tips submenu
               break;
           case 4:
               System.out.println("Swiping card for door.\r\n");
            //    if (NewUser.getId() != null) {
            //     //Do something if the variable is declared.        
            //     }
            //     else {
            //     System.out.println("You haven't created a user to gain access to a room.");
            //     }
             SwipeCard.Swipe(Chris, KS224);
               break;
           case 5:
               System.out.println("You have selected 5...\r\n");
               setNewRoom(); //calling option 5 object and/or methods
               break;
           case 8:
               System.out.println("Returning to main menu....\r\n");
               printMainMenu();
               break;
            case 9:
               System.out.println("You have selected 9.\r\n");
               setGridHeight();
               break;
           default:
               System.out.println("Not a menu option please try again\r\n");
               printMainMenu();
               break;
       }
   }
}
