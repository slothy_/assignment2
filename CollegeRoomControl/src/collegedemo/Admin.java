/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collegedemo;

/**
 *
 * @author sh
 */
public class Admin {
    private static String emergencyText;
    private static final String COLOUR_RED = "\u001B[31m";
    private static final String COLOUR_RESET = "\u001B[0m";
    private static final String COLOUR_GREEN = "\u001B[32m";

    public static void assignRoom(Lecturer lecturer, Room course) {
        lecturer.setCourse(course);
        course.setLecturer(lecturer);
    }
    
    public static void assignRoom(User student, Room course) {
        student.setCourse(course);
    }  
    
    public static void getDetails(CollegePeople p) {
    
        System.out.println("User Name: " + p.getName());
        System.out.println("User ID: " + p.getId());
        System.out.println("User Type: " + p.getUserType());
        
        if (p.getCourse().isEmergency() == true) {
            emergencyText = COLOUR_RED+"Emergency";
        } else {
            emergencyText = COLOUR_GREEN+"Normal";
        }

        if(p.getCourse() != null) {
            System.out.println("Assigned Room: " + p.getCourse().getRoom()+ "(" + emergencyText + COLOUR_RESET+")" + "\n" + "Room Type: " + p.getCourse().getRoomType() + "\n");
        } else {
            System.out.println("No Room assigned");
        }
    }
}
